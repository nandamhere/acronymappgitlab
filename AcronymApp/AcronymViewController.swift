//
//  ViewController.swift
//  AcronymApp
//
//  Created by Vaibhav Nandam on 12/13/22.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var viewModel = AcronymViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        activityIndicator.isHidden = true
        searchBar.delegate = self
    }
    
    private func getAcronym(acronym: String) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        self.viewModel.fetchAcronym(acronym: acronym) { result in
            if result.count == 0 {
                self.showAlert(title: "No data Found", message: "Please try another one")
            }
            self.reloadUI()
        }
    }
    
    private func reloadUI() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
            self.tableView.reloadData()
        }
    }
    
    private func showAlert(title: String, message: String?) {
        DispatchQueue.main.async { let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let action = UIAlertAction(title: "Okay", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "acronymCell")
        cell?.textLabel?.text = viewModel.acronymText(for: indexPath.row)
        cell?.textLabel?.numberOfLines = 0
        return cell ?? UITableViewCell()
    }


}

extension ViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let acronym = searchBar.text, !acronym.isEmpty else {
            showAlert(title: "Enter Acronym", message: nil)
            return
            
        }
        self.getAcronym(acronym: acronym)
        searchBar.resignFirstResponder()
      }
}
