//
//  AcronymViewModel.swift
//  AcronymApp
//
//  Created by Vaibhav Nandam on 12/13/22.
//

import Foundation

class AcronymViewModel {
    var result: Acronym? = nil
    func fetchAcronym(acronym: String, completion: @escaping ([Acronym]) -> Void) {
        var components = URLComponents(string: "http://www.nactem.ac.uk/software/acromine/dictionary.py")
        components?.queryItems = [URLQueryItem(name: "sf", value: acronym)]
        guard let url = components?.url else { return }
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url) { data, response, error in
            if data != nil && error == nil {
                do {
                    let parsingData = try JSONDecoder().decode([Acronym].self, from: data ?? Data())
                    self.result = parsingData.first
                    completion(parsingData)
                } catch {
                    print("Parsing error")
                }
               
            }
        }
        dataTask.resume()
    }
    
    var count: Int {
        return result?.lfs.count ?? 0
    }
    
    func acronymText(for index: Int) -> String? {
        return self.result?.lfs[index].lf
    }
}
